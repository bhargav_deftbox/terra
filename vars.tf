#variable "ami" { default = "ami-0620d12a9cf777c87" }

variable "ami" { default = "ami-0b44050b2d893d5f7" }

variable "instance_type" { default = "t2.micro" }

variable "aws_region" { default = "ap-south-1" }

#variable "instance_count" { default = "1" }

variable "instance_tags" {
  #type = list
  default = "terra-jenkins-wordpress-1"
}

variable "vpc_id" {
  default = "vpc-3e031456"
}

variable "key_name" {
  default = "chatapp"
}

variable "volume_size" {
  description = "EBS volume size in GBs for the instance"
  default     = 8
}

variable "ssh_port" {
  default = "22"
}

variable "http_port" {
  default = "80"
}

variable "mysql_port" {
  default = "3306"
}

variable "nfs_port" {
  default = "2049"
}

variable "allocated_storage" {
  default = "10"
}

variable "instance_class" {
  default = "db.t2.micro"
}

variable "db_admin" {
  default = "dbadmin"
}

variable "db_password" {
  default = "dhinkachika"
}

variable "db_name" {
  default = "dbwordpress"
}
